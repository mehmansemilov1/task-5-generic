class Person {
    private int id;
    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public int getId() {
        return id;
       }

    public void setId(int id) {
        this.id = id;
   }

    @Override
    public String toString() {
        return "ID: " + id + ", Ad: " + name + ", yas: " + age;
    }
}
