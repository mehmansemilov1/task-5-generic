import java.util.ArrayList;
import java.util.List;

class GenericStorage<T extends Person> {
    private List<T> items;
    private int nextId;

    public GenericStorage() {
        items = new ArrayList<>();
        nextId = 1;
    }

    public void addItem(T item) {
        item.setId(nextId++);
        items.add(item);
    }

    public void removeItem(int id) {
        items.removeIf(item -> item.getId() == id);
    }

    public T searchItem(int id) {
        for (T item : items) {
            if (item.getId() == id) {
                return item;
            }
        }
        return null;
    }

    public void displayAllItems() {
        for (T item : items) {
            System.out.println(item);
        }
    }
}