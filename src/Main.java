public class Main {
    public static void main(String[] args) {
        GenericStorage<Person> storage = new GenericStorage<>();

        storage.addItem(new Student("Mehman", 22));
        storage.addItem(new Student("Celebi", 22));
        storage.addItem(new Teacher("Cavidan", 30));
        storage.addItem(new Teacher("Rovshan", 25));
        storage.addItem(new Teacher("Ruslan", 25));

        System.out.println("Butun  Şəxslər:");
        storage.displayAllItems();

        System.out.println("\n Axtarılan Şəxs:");
        Person found = storage.searchItem(1);
        if (found != null) {
            System.out.println("Tapılan : " + found);
        } else {
            System.out.println("Məlumat tapılmadı.");
        }

        System.out.println("\n Yenilen melumat:");
        storage.removeItem(1);
        storage.displayAllItems();
    }
}